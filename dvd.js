//Liaison entre la directive ng-app et la couche métier
var dvdModule = angular.module("dvd",[]);

//Definir le controlleur
dvdModule.controller("listeDeFilm",function listeDeFilmController($scope,$http){
    $scope.modalShower = false;
    $scope.films = response.data;
    
    $scope.film = {
        id: 0,
        titre: "",
        jaquette: "",
        description: ""
    };

    //La fonction requpere la liste des films que retourne le service web et stocke le résultat et le stoke dans la variable film
    $http.get("http://deeveadee.janus-developpement.com/films").then(function (response){
        $scope.films = response.data;
    });
    
    $scope.detail = function(id){
        $scope.film = $scope.getFilm(id);
        $scope.modalShower = true;
    };

    $scope.hideModal = function(){
        $scope.modalShower = false;    
    };

    $scope.getFilm = function(id){
        for(var i in $scope.films){
            if($scope.films[i].id === id){
                return $scope.films[i];
            };
        };
    };

    $scope.updateListe = function(){
        $http.post("http://deeveadee.janus-developpement.com/films", $scope.films).then(function (response){
        alert('test')
        });
    }
    
});

    
    $scope.films = [
        
        {
            id : 1,
            titre : "Le soliste",
            jaquette : "http://cedric974.c.e.pic.centerblog.net/o/93521a00.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 2,
            titre : "Le Ché",
            jaquette : "http://static1.purepeople.com/articles/3/22/53/3/@/154917-l-affiche-du-film-che-guerilla-637x0-2.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        
        {
            id : 3,
            titre : "Tombriser",
            jaquette : "http://www.laboiteverte.fr/wp-content/uploads/2011/10/affiche-film-zombie-06.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 4,
            titre : "Safari",
            jaquette : "http://www.zoom-cinema.fr/media/photos/3530/affiche.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 5,
            titre : "Brice de Nice",
            jaquette : "http://ekladata.com/peaq13XZ6FGZBZBUAqLAjB-rhRg.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 6,
            titre : "Sin city",
            jaquette : "http://img.actucine.com/wp-content/uploads/2014/07/Sin-City-2-140717-03.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 7,
            titre : "E.T",
            jaquette : "http://images.affiches-et-posters.com/albums/3/2979/2979-affiche-film-e-t-l-extra-terrestre.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 8,
            titre : "Vadrouille",
            jaquette : "http://fr.web.img5.acsta.net/pictures/16/06/16/12/01/072037.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 9,
            titre : "POMPEI",
            jaquette : "http://www.lyricis.fr/wp-content/uploads/2014/01/POMPEI-AFFICHE-FRANCE-FINALE.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 10,
            titre : "Le pari",
            jaquette : "https://media.senscritique.com/media/000008232678/source_big/Le_Pari.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 11,
            titre : "La momie",
            jaquette : "https://media.senscritique.com/media/000016930713/source_big/La_Momie.jpg",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
        
        {
            id : 12,
            titre : "Les chit's",
            jaquette : "",
            description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus."
        },
    ];



